﻿using System;
using System.Collections.Generic;
using PruebatecnicaHorus.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PruebatecnicaHorus.Views
{
	[QueryProperty(nameof(Token), "token")]
	public partial class GamificationView : ContentPage
	{
		ChallengeViewModel vm;

		public string Token
		{
			set
			{
				vm.LoadCallegens(value);
			}
		}

		public GamificationView ()
		{
			InitializeComponent ();
			BindingContext = vm = StartUp.Resolve<ChallengeViewModel>();
		}
	}
}

