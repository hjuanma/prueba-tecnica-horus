﻿using System;
using System.Collections.Generic;
using PruebatecnicaHorus.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PruebatecnicaHorus.Views
{	
	public partial class LoginView : ContentPage
	{
        LoginViewModel vm;
		public LoginView()
		{
			InitializeComponent ();

			BindingContext = vm = StartUp.Resolve<LoginViewModel>();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var aux = Preferences.ContainsKey("UserToken");
            if (aux)
            {
                await Shell.Current.GoToAsync($"//{nameof(GamificationView)}?token={Preferences.Get("UserToken",string.Empty)}");
            }
        }
    }
}

