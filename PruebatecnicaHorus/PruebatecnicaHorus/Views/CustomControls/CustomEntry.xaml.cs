﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace PruebatecnicaHorus.Views.CustomControls
{	
	public partial class CustomEntry : ContentView
	{

		public static readonly BindableProperty PlaceHolderProperty =
			BindableProperty.Create(nameof(PlaceHolder), typeof(string), typeof(CustomEntry), string.Empty);

		public string PlaceHolder
		{
			get { return (string)GetValue(PlaceHolderProperty); }
			set { SetValue(PlaceHolderProperty, value); }
		}

		public static readonly BindableProperty TextProperty =
			BindableProperty.Create(nameof(Text), typeof(string), typeof(CustomEntry), string.Empty);

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public static readonly BindableProperty IsPassProperty =
			BindableProperty.Create(nameof(IsPass), typeof(bool), typeof(CustomEntry), false);

		public bool IsPass
		{
			get { return (bool)GetValue(IsPassProperty); }
			set { SetValue(IsPassProperty, value); }
		}

		public CustomEntry ()
		{
			InitializeComponent ();
		}

        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
			entry.IsPassword = !entry.IsPassword;
			entry.Focus();

		}
    }
}

