﻿using System;
using System.Collections.Generic;
using Acr.UserDialogs;
using PruebatecnicaHorus.Models;
using Xamarin.Forms;

namespace PruebatecnicaHorus.Views.CustomControls
{
	public partial class ChallengeItemView : ContentView
	{

		public static readonly BindableProperty ChallengProperty =
			BindableProperty.Create(nameof(Challeng), typeof(ChallengModel), typeof(ChallengeItemView), new ChallengModel(), propertyChanged: OnChallengChanged);


        public ChallengModel Challeng
		{
			get { return (ChallengModel)GetValue(ChallengProperty); }
			set
			{
				SetValue(ChallengProperty, value);
			}
		}

		public ChallengeItemView ()
		{
			InitializeComponent ();
		}

		private static void OnChallengChanged(BindableObject bindable, object oldValue, object newValue)
		{
			if (oldValue == newValue || newValue == null)
			{
				return;
			}
			var challeng = (ChallengModel)newValue;
			var content = (ChallengeItemView)bindable;
			content.ProgressBar.Progress = (double)challeng.CurrentPoints / (double)challeng.TotalPoints;
			content.Percent.Text = ((double)challeng.CurrentPoints / (double)challeng.TotalPoints).ToString("P0");
			string visualState = challeng.CurrentPoints != challeng.TotalPoints ? "Default" : "Done";
			VisualStateManager.GoToState(content.container, visualState);
		}

        async void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
			await UserDialogs.Instance.AlertAsync(Challeng.Description, Challeng.Title, "OK");
		}
	}
}

