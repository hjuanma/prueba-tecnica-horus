﻿using System;
using Microsoft.Extensions.DependencyInjection;
using PruebatecnicaHorus.Services;
using PruebatecnicaHorus.ViewModels;
using Xamarin.Essentials;

namespace PruebatecnicaHorus
{
    public static class StartUp
	{
		private static IServiceProvider serviceProvider;
		private const string baseUri = "https://horuschallenges.azurewebsites.net/api/";


		public static void ConfigurateServices()
        {
			var services = new ServiceCollection();

			services.AddHttpClient<ILoginService, ApiLoginService>(c =>
			{
				c.BaseAddress = new Uri(baseUri);
			});

			services.AddHttpClient<IChallengesService, ApiChallengesService>(c =>
			{
				c.BaseAddress = new Uri(baseUri);
				//c.DefaultRequestHeaders.Add("Authorization", Preferences.Get("UserToken", string.Empty));
			});

			services.AddTransient<LoginViewModel>();
			services.AddTransient<ChallengeViewModel>();

			serviceProvider = services.BuildServiceProvider();
		}

		public static T Resolve<T>() => serviceProvider.GetService<T>();
	}
}

