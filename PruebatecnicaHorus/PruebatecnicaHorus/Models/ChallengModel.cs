﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace PruebatecnicaHorus.Models
{
	public class ChallengModel
	{
		[JsonPropertyName("id")]
		public string Id { get; set; }

		[JsonPropertyName("title")]
		public string Title { get; set; }

		[JsonPropertyName("description")]
		public string Description { get; set; }

		[JsonPropertyName("currentPoints")]
		public long CurrentPoints { get; set; }

		[JsonPropertyName("totalPoints")]
		public long TotalPoints { get; set; }
	}
}

