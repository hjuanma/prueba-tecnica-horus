﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace PruebatecnicaHorus.Models
{
	public class UserModel
	{

		[JsonPropertyName("email")]
		public string Email { get; set; }

		[JsonPropertyName("firstname")]
		public string Firstname { get; set; }

		[JsonPropertyName("surname")]
		public string Surname { get; set; }

		[JsonPropertyName("authorizationToken")]
		public string AuthorizationToken { get; set; }
	}
}

