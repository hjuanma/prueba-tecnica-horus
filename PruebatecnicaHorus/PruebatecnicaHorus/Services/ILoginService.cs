﻿using System;
using System.Threading.Tasks;
using PruebatecnicaHorus.Models;

namespace PruebatecnicaHorus.Services
{
	public interface ILoginService
	{
		Task<UserModel> GetUser(string email, string password);
	}
}

