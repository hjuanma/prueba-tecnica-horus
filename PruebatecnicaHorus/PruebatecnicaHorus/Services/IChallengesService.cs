﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PruebatecnicaHorus.Models;

namespace PruebatecnicaHorus.Services
{
	public interface IChallengesService
	{
		Task<IList<ChallengModel>> GetChallenge();
	}
}

