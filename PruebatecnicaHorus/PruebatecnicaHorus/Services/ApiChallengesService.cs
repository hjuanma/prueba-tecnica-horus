﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using PruebatecnicaHorus.Models;
using Xamarin.Essentials;

namespace PruebatecnicaHorus.Services
{
    public class ApiChallengesService : IChallengesService
    {
        private readonly HttpClient _client;

        public ApiChallengesService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IList<ChallengModel>> GetChallenge()
        {
            try
            {
                _client.DefaultRequestHeaders.Clear();
                _client.DefaultRequestHeaders.Add("Authorization", Preferences.Get("UserToken", string.Empty));
                var response = await _client.GetAsync("Challenges");
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                return JsonSerializer.Deserialize<List<ChallengModel>>(responseString);
            }
            catch (Exception ex)
            {
                return new List<ChallengModel>();
            }
        }
    }
}

