﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using PruebatecnicaHorus.Models;

namespace PruebatecnicaHorus.Services
{
	public class ApiLoginService : ILoginService
    {
        private readonly HttpClient _client;

        public ApiLoginService(HttpClient client)
		{
            _client = client;
        }

        public async Task<UserModel> GetUser(string email, string password)
        {
            try
            {
                var content = JsonSerializer.Serialize(new { Email = email, Password = password });
                var data = new StringContent(content, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync("UserSignIn", data);

                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                return JsonSerializer.Deserialize<UserModel>(responseString);
            }
            catch (Exception ex)
            {
                return new UserModel();
            }
        }
    }
}

