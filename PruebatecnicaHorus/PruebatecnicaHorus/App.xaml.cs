﻿using System;
using Xamarin.Forms;

namespace PruebatecnicaHorus
{
    public partial class App : Application
    {

        public App ()
        {
            InitializeComponent();
            StartUp.ConfigurateServices();
            MainPage = new AppShell();
        }

        protected override void OnStart ()
        {
        }

        protected override void OnSleep ()
        {
        }

        protected override void OnResume ()
        {
        }
    }
}

