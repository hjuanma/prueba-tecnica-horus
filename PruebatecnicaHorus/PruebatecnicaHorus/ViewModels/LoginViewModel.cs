﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using Acr.UserDialogs;
using PruebatecnicaHorus.Models;
using PruebatecnicaHorus.Views;
using PruebatecnicaHorus.Services;
using Xamarin.Essentials;

namespace PruebatecnicaHorus.ViewModels
{
	public class LoginViewModel : BaseViewModel
	{
        private ApiLoginService api;

        private readonly ILoginService _loginService;

        private ICommand _showAlert;
        public ICommand ShowAlert
        {
            get
            {
                return this._showAlert;
            }

            set
            {
                if (value != this._showAlert)
                {
                    this._showAlert = value;
                    SetProperty();
                }
            }
        }

        private string _email;
        public string Email
        {
            get
            {
                return this._email;
            }

            set
            {
                if (value != this._email)
                {
                    this._email = value;
                    SetProperty();
                }
            }

        }

        private string _passWord;
        public string PassWord
        {
            get
            {
                return this._passWord;
            }

            set
            {
                if (value != this._passWord)
                {
                    this._passWord = value;
                    SetProperty();
                }
            }

        }

        public LoginViewModel(ILoginService loginService)
		{
            _loginService = loginService;

           ShowAlert = new Command<LoginActions>(async (act) =>
           {
               switch (act)
               {
                   case LoginActions.Login:
                       Login();
                       break;
                   case LoginActions.SingUp:
                   case LoginActions.FaceBook:
                   case LoginActions.Instagram:
                       await UserDialogs.Instance.AlertAsync("", act.ToString(), "OK");
                       break;
                   default:
                       break;
               }

           });
        }

        private async void Login()
        {
            var user = await _loginService.GetUser(Email, PassWord);

            if (!string.IsNullOrEmpty(user.AuthorizationToken) )
            {

                await Shell.Current.GoToAsync($"//{nameof(GamificationView)}?token={user.AuthorizationToken}");

            }
            else
            {
                await UserDialogs.Instance.AlertAsync("", "Login error", "OK");
            }
        }
    }
}

