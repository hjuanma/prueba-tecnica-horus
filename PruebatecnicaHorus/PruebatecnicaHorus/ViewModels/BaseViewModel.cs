﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PruebatecnicaHorus.ViewModels
{
	public class BaseViewModel : INotifyPropertyChanged
	{
		public BaseViewModel()
		{
		}

        public event PropertyChangedEventHandler PropertyChanged;
		protected void SetProperty([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

