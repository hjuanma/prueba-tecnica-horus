﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Windows.Input;
using PruebatecnicaHorus.Models;
using PruebatecnicaHorus.Services;
using PruebatecnicaHorus.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PruebatecnicaHorus.ViewModels
{
    public class ChallengeViewModel : BaseViewModel
    {

        private ApiLoginService api;

        private readonly IChallengesService _challengesService;

        private ICommand _backCommand;
        public ICommand BackCommand
        {
            get
            {
                return this._backCommand;
            }

            set
            {
                if (value != this._backCommand)
                {
                    this._backCommand = value;
                    SetProperty();
                }
            }
        }

        private int _challengesDone;
        public int ChallengesDone
        {
            get
            {
                return this._challengesDone;
            }

            set
            {
                if (value != this._challengesDone)
                {
                    this._challengesDone = value;
                    SetProperty();
                }
            }
        }

        private ObservableCollection<ChallengModel> _challengesList;
        public ObservableCollection<ChallengModel> ChallengesList
        {
            get
            {
                return this._challengesList;
            }

            set
            {
                if (value != this._challengesList)
                {
                    this._challengesList = value;
                    SetProperty();
                }
            }
        }

        public ChallengeViewModel(IChallengesService challengesService)
        {

            _challengesService = challengesService;

            BackCommand = new Command(async () =>
            {
                Preferences.Remove("UserToken");
                await Shell.Current.GoToAsync($"//{nameof(LoginView)}");
            });
        }

        public async void LoadCallegens(string value)
        {
            Preferences.Set("UserToken", value);
            ChallengesList = new ObservableCollection<ChallengModel>( await _challengesService.GetChallenge());
            ChallengesDone = ChallengesList.Where(c => c.TotalPoints == c.CurrentPoints).Count();
        }
    }
}

